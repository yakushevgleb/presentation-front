import React, { Component } from 'react';
import { IndexLink, Link } from 'react-router';

export default class Menu extends Component {

  render() {
    return (
      <div className='Menu'>
        <div className="transp-bg">
        </div>
        <div className="controls">
          {this.props.isSaved ? (
            <span className="saved">Сохранено</span>
          ) : (
            <button className="save" onClick={this.props.save}>Сохранить</button>
          )}

        </div>
      </div>
    );
  }
}
