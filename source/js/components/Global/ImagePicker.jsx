import React, { Component } from 'react';
import Config from 'Config';

export default class ImagePicker extends Component {

  constructor(props) {
    super(props);
    this.state = {
      modalOpened: false,
      choosenColor: '#f6f6f6',
      isSelected: false
    };
    this.openModal = this.openModal.bind(this);
  }

  openModal(){
    this.setState({modalOpened: !this.state.modalOpened});
  }

  itemSelected(item, index, event){
    item.show = !item.show;
    this.props.itemSelected(item, index, event);
  }

  render() {
    return (
      <div className='AddObject'>
          <button className="change" onClick={this.openModal}>Изменить</button>
          {this.state.modalOpened &&
            <div className="wrapper">
              <div className="modal-background" onClick={this.openModal}>
              </div>
              <div className="modal-window" style={{width: '800px', left: '20%'}}>
              <div className="close" onClick={this.openModal}>+</div>
              <div className="content">
                {this.props.list.map((item, index)=>
                  <div key={index} className="img-box" onClick={this.itemSelected.bind(this, item, index)}>
                  <div className={"choose" + (item.show ? " choosen" : "")}></div>
                    <div className="img-hover">
                      <img src={`${Config.address}/api/presentations/data/image_crop?url=${Config.photoAddress + item.url}&width=280&height=315`} />
                    </div>
                  </div>
                )}
              </div>
              </div>
            </div>
          }
      </div>
    );
  }
}
