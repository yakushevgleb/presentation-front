import React, { Component } from 'react';
import ToggledInput from "./ToggledInput";

import phone from '../../../assets/img/icons/phone.png'
import human from '../../../assets/img/icons/human.png'
import logo from "../../../assets/img/logo.png"

export default class AddObject extends Component {

  constructor(props) {
    super(props);
    this.state = {modalOpened: false}
    this.openModal = this.openModal.bind(this);
  }

  componentDidMount() {
    this.setState({pageCounter: this.props.pageCounter})
  }

  openModal(){
    this.setState({modalOpened: !this.state.modalOpened});
  }

  itemSelected(fieldName, index){
    let list = this.props.list;
    let emptyCounter = 0;
    list.forEach(item=>{
      if (!item.value) {
        emptyCounter++;
      }
    });
    if (emptyCounter === 1) {
      this.setState({modalOpened: false});
    }
    this.props.itemSelected(fieldName, index);
  }

  render() {
    return (
      <div className='AddObject'>
        <div className="addButton" onClick={this.openModal}>
          <span className="plus">+</span>
        </div>
        {this.state.modalOpened &&
          <div className="wrapper">
            <div className="modal-background" onClick={this.openModal}>
            </div>
            <div className="modal-window">
            <div className="close" onClick={this.openModal}>+</div>
              {this.props.listName === "props" &&
                <div className="props">
                {this.props.list.map((item, index)=>{
                  if(!item.value) {
                    return (<div className="prop selected" key={index} onClick={this.itemSelected.bind(this, item.fieldName, undefined)}>
                             <div className="prop-image"><img src={item.icon} /></div>
                              <div className="prop-name">
                                <div className="prop-name-value">{item.value}</div>
                                <div className="prop-name-text">{item.propName}</div>
                              </div>
                            </div>)
                  }
                })}
                </div>
              }
              {this.props.listName === "offer" &&
                <div>
                {this.props.list.map((item, index)=>{
                  if(!item.val) {
                    return (<div className="pointer" onClick={this.itemSelected.bind(this, "offer", index)}>
                              {item.key}
                            </div>)
                  }
                }
                )}
                </div>
              }
            </div>
          </div>
        }
      </div>
    );
  }
}
