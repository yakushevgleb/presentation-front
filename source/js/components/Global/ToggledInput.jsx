import React, { Component } from 'react';
import { IndexLink, Link } from 'react-router';

export default class ToggledInput extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isInpHidden: true,
      value: ""
    };
    this.toggleProp = this.toggleProp.bind(this);
  }

  componentWillMount() {
    this.setState({
      value: this.props.value
    });

  }

  toggleProp() {
    this.setState({
      isInpHidden: !this.state.isInpHidden,
    });
  }

  onChange(name, index, arrName, event) {
    this.setState({value: event.target.value});
    this.props.onChange(name, index, arrName, event);
  }

  render() {

    let showInp = null;

    if (this.state.isInpHidden && this.props.value) {
      showInp = <span className="ToggledInput" onClick={this.toggleProp}>{this.state.value}</span>
    } else {
      showInp = <input value={this.state.value} placeholder={this.props.placeholder ? this.props.placeholder : "Не задано"} type="text" onChange={this.onChange.bind(this, this.props.fieldName, this.props.index, this.props.arrFieldName)} onBlur={this.toggleProp} autoFocus />
    }

    if (this.state.isInpHidden && !this.props.value) {
      showInp = <input value={this.state.value} placeholder={this.props.placeholder ? this.props.placeholder : "Не задано"} type="text" onChange={this.onChange.bind(this, this.props.fieldName, this.props.index, this.props.arrFieldName)} onBlur={this.toggleProp} />
    }

    return (
      <span>{showInp}</span>
    );
  }
}
