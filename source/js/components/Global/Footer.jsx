import React, { Component } from 'react';
import TextDate from "./TextDate";

import phone from '../../../assets/img/icons/phone.png'
import human from '../../../assets/img/icons/human.png'
import logo from "../../../assets/img/logo.png"

export default class Footer extends Component {

  constructor(props) {
    super(props);
    this.state = {pageCounter: 1}
  }

  componentDidMount() {
    this.setState({pageCounter: this.props.pageCounter})
  }

  render() {
    return (
      <div className='Footer'>
        <div className='phone-numbers'>
          <div className="city-number"><img src={phone} /><span>+7 (495) 646 13 46</span></div>
          <div className="mobile-number"><img src={human} /><span>{this.props.broker.phone} {this.props.broker.name}</span></div>
        </div>
        <div className="logo">
          <img src={logo} />
        </div>
        <div className="date-page">
          <div className="date">
            <TextDate />
          </div>
          <div className="page">
            <span>{this.state.pageCounter}</span>
          </div>
        </div>
      </div>
    );
  }
}
