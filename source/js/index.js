import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import 'babel-polyfill';
import logger from 'dev/logger';
import { Router, Route, Switch, browserHistory } from 'react-router'

import rootReducer from 'reducers';
import Routes from 'routes';

import App from 'views/App';

// Load SCSS
import '../scss/app.scss';

ReactDOM.render(
  (
    <Router history={browserHistory} >
      <Route path="/:id" component={App} />
    </Router>
  ),
  document.getElementById('root')
);
