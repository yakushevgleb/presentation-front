import React, { Component } from 'react';
import Config from 'Config';

import ToggledInput from '../../components/Global/ToggledInput';
import AddObject from "../../components/Global/AddObject";

import bigBld from "../../../assets/img/icons/big_bld.png";
import addrIcon from "../../../assets/img/icons/addr.png";
import metroIcon from "../../../assets/img/icons/metro.png";
import bluePath from "../../../assets/img/icons/blue_path.png";
import abc from "../../../assets/img/icons/abc.png";
import dogovor from "../../../assets/img/icons/dogovor.png";
import gear from "../../../assets/img/icons/gear.png";
import goal from "../../../assets/img/icons/goal.png";
import lift from "../../../assets/img/icons/lift.png";
import layer from "../../../assets/img/icons/layer.png";
import pdf from "../../../assets/img/icons/pdf.png";
import photo from "../../../assets/img/icons/photo.png";
import check from "../../../assets/img/icons/v.png";
import pen from "../../../assets/img/icons/pen.png";
import plus from "../../../assets/img/icons/plus.png";
import parkingIcon from "../../../assets/img/icons/parking.png";
import floorsIcon from "../../../assets/img/icons/bld_icon.png";
import craneIcon from "../../../assets/img/icons/crane.png";
import rulerIcon from "../../../assets/img/icons/ruler.png";
import taxIcon from "../../../assets/img/icons/tax_icon.png";

export default class AboutObject extends Component {

  constructor(props) {
    super(props);
    this.state = {
      showText: true,
      propsArr: []
    }
    this.onChange = this.onChange.bind(this);
    this.showInp = this.showInp.bind(this);
    this.itemSelected = this.itemSelected.bind(this);
  }

  componentDidMount() {
    if(this.props.model.infrastructures) {
      this.setState({showText: true});
    } else {
      this.setState({showText: false});
    }
    this.setState({
      propsArr: [
        {
          value: this.props.model.bc_class,
          icon: abc,
          fieldName: "bc_class",
          propName: "Класс"
        },
        {
          value: this.props.model.year,
          icon: craneIcon,
          fieldName: "year",
          propName: "Год постройки"
        },
        {
          value: this.props.model.area,
          icon: goal,
          fieldName: "area",
          propName: "Общая площадь"
        },
        {
          value: this.props.model.floors,
          icon: floorsIcon,
          fieldName: "floors",
          propName: "Этажность"
        },
        {
          value: (this.props.model.lift_company == "1") ? "Лифт" : this.props.model.lift_company,
          icon: lift,
          fieldName: "lift_company",
          propName: this.props.model.passenger_elevators
        },
        {
          value: this.props.model.land_area,
          icon: rulerIcon,
          fieldName: "land_area",
          propName: "Площадь участка"
        },
        {
          value: this.props.model.rent_contract,
          icon: dogovor,
          fieldName: "rent_contract",
          propName: "Договор аренды"
        },
        {
          value: this.props.model.taxes_department,
          icon: taxIcon,
          fieldName: "taxes_department",
          propName: "Налоговая"
        }
      ]
    })
  }

  onChange(name, index, arrName, event) {
    let buildings = this.props.buildings;
    if(!arrName) {
      buildings[this.props.index].building[name] = event.target.value;
    } else {
      if (name === "blocks") {
        buildings[this.props.index][name][index][arrName] = event.target.value;
      } else {
        buildings[this.props.index].building[name][index][arrName] = event.target.value;
      }
    }

    this.props.changedData({buildings});
  }

  itemSelected(fieldName, index) {
    let buildings = this.props.buildings;
    if (fieldName && index >= 0) {
      buildings[this.props.index].building[fieldName][index].val = "Неизвестно";
    } else {
      buildings[this.props.index].building[fieldName] = "Неизвестно";
    }
    this.props.changedData({buildings});
    this.componentDidMount();
  }

  deleteItem(fieldName, index, deleteFromArr){
    let buildings = this.props.buildings;
    if(index >= 0) {
      if (deleteFromArr) {
        let arr = buildings[this.props.index].building[fieldName];
        arr.splice(index, 1);
        buildings[this.props.index].building[fieldName] = arr;
      } else {
        buildings[this.props.index].building[fieldName][index].val = null;
      }
    } else {
      buildings[this.props.index].building[fieldName] = undefined;
    }
    this.props.changedData({buildings});
    this.componentDidMount();
  }

  addItem(fieldName, index) {
    let buildings = this.props.buildings;
    let arr = buildings[this.props.index].building[fieldName];
    arr.push({
      count: null,
      cur: 1,
      id: 1,
      price: null,
      val: "Неизвестно"
    });
    buildings[this.props.index].building[fieldName] = arr;
    this.props.changedData({buildings});
    this.componentDidMount();
  }

  showInp() {
    if(this.props.model.infrastructures) {
      this.setState({showText: !this.state.showText});
    } else {
      this.setState({showText: false});
    }
  }

  render() {
    let index = this.props.index + 1;
    let model = this.props.model;
    let blocks = this.props.blocks;
    let infrastructures = model.infrastructures.split(',');
    let parking = model.parking_json;


    return (
      <div className="AboutObject">
        <div className="header">
          <div className="logo">
            <img src={bigBld} />
            <span>№{index}</span>
          </div>
          <div className="title-area">
            <div className="title thin"><ToggledInput value={this.props.model.name} fieldName="name" onChange={this.onChange} /></div>
            <div className="address thin"><img src={addrIcon} /><span><ToggledInput value={this.props.model.address} fieldName="address" onChange={this.onChange} /></span></div>
            <div className="object-type"><span className="bold">Тип объекта</span> <span className="bc"><ToggledInput value={this.props.model.prefix} fieldName="prefix" onChange={this.onChange} /></span></div>
          </div>
          <div className="metro-area">
            {model.stations.map((station, index)=>{
              if (index < 2) {
                return (
                  <div className="metro" key={index}>
                    <div className="name-logo">
                      <img src={metroIcon} /> <span className="thin"><ToggledInput value={station.name} fieldName="stations" index={index} arrFieldName="name" onChange={this.onChange} /></span>
                    </div>
                    <div className="info">
                      <img src={bluePath} /><img src={addrIcon} /><span className="thin"><ToggledInput value={station.time} fieldName="stations" index={index} arrFieldName="time" onChange={this.onChange} /> мин <ToggledInput value={station.type} fieldName="stations" index={index} arrFieldName="type" onChange={this.onChange} /></span>
                    </div>
                  </div>
                )
              }
            })}
          </div>
        </div>

        {this.props.showFullInfo ? (
          <div className="object-info">
            <div className="image">
             <img src={`${Config.address}/api/presentations/data/image_crop?url=${model.first_image}&width=495&height=278`} />
            </div>
            <div className="properties">
            <AddObject list={this.state.propsArr} listName="props" itemSelected={this.itemSelected} />
             <div className="props">

             {this.state.propsArr.map((prop, index)=>{
               if(prop.value) {
                 if(prop.fieldName === "lift_company") {
                   return (<div className="prop" key={index}>
                             <div className="close" onClick={this.deleteItem.bind(this, prop.fieldName)}>+</div>
                              <div className="prop-image"><img src={prop.icon} /></div>
                               <div className="prop-name">
                                 <div className="prop-name-value"><ToggledInput value={prop.value} fieldName={prop.fieldName} onChange={this.onChange} /></div>
                                 <div className="prop-name-text">
                                  {(prop.fieldName === "lift_company") ? (
                                    <ToggledInput value={prop.propName} placeholder="Количество лифтов" fieldName="passenger_elevators" onChange={this.onChange} />
                                  ) : (
                                    <span>{prop.propName}</span>
                                  )}
                                 </div>
                               </div>
                             </div>)
                 }
                 return (<div className="prop" key={index}>
                          <div className="close" onClick={this.deleteItem.bind(this, prop.fieldName)}>+</div>
                          <div className="prop-image"><img src={prop.icon} /></div>
                          <div className="prop-name">
                            <div className="prop-name-value"><ToggledInput value={prop.value} fieldName={prop.fieldName} onChange={this.onChange} /></div>
                            <div className="prop-name-text">{prop.propName}</div>
                          </div>
                         </div>)
               }

             })}

             </div>


             <div className="infrastructure">
              <div className="infrastructure-title">
                <img src={check} /> <span>Инфраструктура</span>
                {this.state.showText ? (
                  <div className="infrastructure-values" onClick={this.showInp}>
                    {infrastructures.map((item, index)=>{
                      return <span className="ToggledInput" key={index}>{item}</span>
                    })}
                  </div>
                ) : (
                  <div className="infrastructure-values">
                    {model.infrastructures ? (
                      <input value={model.infrastructures} type="text" placeholder="Введите через запятую" onBlur={this.showInp} onChange={this.onChange.bind(this, "infrastructures", null, null)} autoFocus />
                    ) : (
                      <input value={model.infrastructures} type="text" placeholder="Введите через запятую" onBlur={this.showInp} onChange={this.onChange.bind(this, "infrastructures", null, null)} />
                    )}

                  </div>
                )}

              </div>
             </div>
            </div>
            <div className="description">
              <div className="technical">
               <div className="technical-title">
                <img src={gear} /> <span>Технические характеристики</span>
               </div>
               {model.features.map((feature, index)=>{
                 if (feature.key !== "Парковка") {
                   return (<div className="feature" key={index}>
                              <span>{feature.key}</span>
                              <span className="val">
                                <ToggledInput value={this.props.model.features[index].val} fieldName="features" index={index} arrFieldName="val" onChange={this.onChange} />
                              </span>
                            </div>)
                 }
               })}
              </div>
              <div className="commercial">
                <div className="commercial-title">
                  <AddObject list={model.offer} listName="offer" itemSelected={this.itemSelected} />
                  <img src={pen} /> <span>Коммерческие условия</span>
                </div>
                <div className="parking">
                  {model.offer.map((item, index)=>{
                    if(item.val !== null) {
                      return (
                        <div key={index}>
                          {item.key}
                          <span className="blue-text">
                            <ToggledInput value={item.val} fieldName="offer" index={index} arrFieldName="val" onChange={this.onChange} />
                          </span>
                          <span className="delete" onClick={this.deleteItem.bind(this, "offer", index, false)}>+</span>
                        </div>)
                    }
                  }
                  )}
                </div>
                <div className="parking">
                  <div>Паркинг:</div>
                  {parking.map((item, index)=>{
                    return <span key={index}>
                    <img src={parkingIcon} />
                    <span className="blue-text">
                      <ToggledInput value={item.val} fieldName="parking_json" index={index} arrFieldName="val" onChange={this.onChange} />
                    </span>
                    <span>
                      <ToggledInput value={item.price} placeholder="введите цену" fieldName="parking_json" index={index} arrFieldName="price" onChange={this.onChange} />
                      <select className="curr-selector" value={item.cur} onChange={this.onChange.bind(this, "parking_json", index, "cur")}>
                        <option value="1">руб.</option>
                        <option value="2">&euro;</option>
                        <option value="3">$</option>
                      </select>/мес.
                    </span>
                    <span className="delete" onClick={this.deleteItem.bind(this, "parking_json", index, true)}>+</span>
                    </span>
                  })}
                  <span className="add-item blue-text pointer" onClick={this.addItem.bind(this, "parking_json", index)}>+</span>
                </div>
              </div>
            </div>
          </div>
        ) : (
          <div className="object-info">
            <div className="image">
             <img src={`${Config.address}/api/presentations/data/image_crop?url=${model.first_image}&width=495&height=278`} />
            </div>
            <div className="suggested-areas">
              <div className="suggested-areas-title">
                <img src={plus} /> <span>Предлагаемые площади</span>
              </div>
              <div className="suggested-areas-table">
                <table>
                  <tbody>
                    <tr className="table-header">
                      <td className="table-floor">Этаж</td>
                      <td className="table-area">Площадь м<sup>2</sup></td>
                      <td className="table-price">{(this.props.orderType == "rent") ? (<span>Ставка р./м<sup>2</sup> в год</span>) : (<span>Стоимость р./м<sup>2</sup></span>)}</td>
                      {this.props.orderType == "rent" &&
                        <td className="table-expense">Эксп. расх. р./м<sup>2</sup>  в год</td>
                      }
                      <td className="table-tax">Налоги</td>
                      <td className="table-comment">Комментарий</td>
                    </tr>
                    {blocks.map((block, index)=>{
                      if(index > 4) {
                        return (<tr key={index}>
                                  <td><ToggledInput value={block.floors} fieldName="blocks" index={index} arrFieldName="floors" onChange={this.onChange} /></td>
                                  <td><ToggledInput value={block.area} fieldName="blocks" index={index} arrFieldName="area" onChange={this.onChange} /></td>
                                  <td><ToggledInput value={block.price} fieldName="blocks" index={index} arrFieldName="price" onChange={this.onChange} /></td>
                                  {this.props.orderType == "rent" &&
                                    <td><ToggledInput value={block.utilities_costs} fieldName="blocks" index={index} arrFieldName="utilities_costs" onChange={this.onChange} /></td>
                                  }
                                  <td><ToggledInput value={block.taxes} fieldName="blocks" index={index} arrFieldName="taxes" onChange={this.onChange} /></td>
                                  <td><ToggledInput value={block.condition} fieldName="blocks" index={index} arrFieldName="condition" onChange={this.onChange} /></td>
                                </tr>)
                      }
                    })}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        )}


        {this.props.showFullInfo ? (
          <div className="object-info">
            <div className="image">
            </div>
            <div className="suggested-areas">
              <div className="suggested-areas-title">
                <img src={plus} /> <span>Предлагаемые площади</span>
              </div>
              <div className="suggested-areas-table">
                <table>
                  <tbody>
                    <tr className="table-header">
                      <td className="table-floor">Этаж</td>
                      <td className="table-area">Площадь м<sup>2</sup></td>
                      <td className="table-price">{(this.props.orderType == "rent") ? (<span>Ставка р./м<sup>2</sup> в год</span>) : (<span>Стоимость р./м<sup>2</sup></span>)}</td>
                      {this.props.orderType == "rent" &&
                        <td className="table-expense">Эксп. расх. р./м<sup>2</sup>  в год</td>
                      }
                      <td className="table-tax">Налоги</td>
                      <td className="table-comment">Комментарий</td>
                    </tr>
                    {blocks.map((block, index)=>{
                      if(index <= 4) {
                        return (<tr key={index}>
                                  <td><ToggledInput value={block.floors} fieldName="blocks" index={index} arrFieldName="floors" onChange={this.onChange} /></td>
                                  <td><ToggledInput value={block.area} fieldName="blocks" index={index} arrFieldName="area" onChange={this.onChange} /></td>
                                  <td><ToggledInput value={block.price} fieldName="blocks" index={index} arrFieldName="price" onChange={this.onChange} /></td>
                                  {this.props.orderType == "rent" &&
                                    <td><ToggledInput value={block.utilities_costs} fieldName="blocks" index={index} arrFieldName="utilities_costs" onChange={this.onChange} /></td>
                                  }
                                  <td><ToggledInput value={block.taxes} fieldName="blocks" index={index} arrFieldName="taxes" onChange={this.onChange} /></td>
                                  <td><ToggledInput value={block.condition} fieldName="blocks" index={index} arrFieldName="condition" onChange={this.onChange} /></td>
                                </tr>)
                      }
                    })}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        ) : (
          <div className="object-info">
          </div>
        )}


        <div className="additional">
          <div className="additional-top">
          <table>
            <tbody>
              <tr>
                <td className="additional-information">
                  Дополнительную информацию предлагаем посмотреть на сайте
                </td>
                <td>
                  <div className="site"><a href={`http://of.ru/bc/${model.crm_id}`} target="_blank"><span className="bold">of.ru</span><span className="blue-text">/bc/{model.crm_id}</span></a></div>
                </td>
                <td className="manager-request">
                  или запросить у Вашего менеджера
                </td>
                <td>
                  <div className="manager-id"><div className="id-icon">ID</div><span className="blue-text">{model.id}</span></div>
                </td>
                <td className="comment">
                  <span><div className="comment-title light-blue">Комментарий</div><div className="comment-content"><ToggledInput value={model.presentation_comment} fieldName="presentation_comment" onChange={this.onChange} /></div></span>
                </td>
                <td className="download-presentation">
                </td>
              </tr>
            </tbody>
          </table>
          </div>
          <div className="additional-bottom">
            Предложение не является публичной офертой. Вышеуказанные условия могут быть изменены и являются предметом переговоров
          </div>
        </div>
      </div>
    );
  }
}
