import React, { Component } from 'react';
import buildingIcon from "../../../assets/img/icons/building.png";
import addrIcon from "../../../assets/img/icons/addr.png";
import metroIcon from "../../../assets/img/icons/metro.png";

import Footer from "../../components/Global/Footer";

export default class SuggestedObjects extends Component {


  render() {
    let buildings = this.props.model;

    return (
      <div className='SuggestedObjects'>
        <div className="page-title">
          <img src={buildingIcon} id="bld-icon" /> <span className="thin">Предлагаемые</span> <span className="bold">объекты</span>
        </div>
        <div className="suggested-list">
        {buildings.map((building, index)=>{
          if (index < 10) {
            return (<div className="list-item" key={index}>
                    <div className="counter"><span className="digit">{index + 1}</span></div>
                    <div className="object">
                      <div className="photo">
                       <img src={building.building.first_image_avatar} />
                      </div>
                      <div className="description">
                        <div className="object-title">
                          <div className="bld-class">{building.building.bc_class}</div>
                          <div className="bld-name">{building.building.name}</div>
                        </div>
                        <div className="address">
                          <div className="address-icon"><img src={addrIcon} /></div>
                          <div className="address-name">{building.building.address}</div>
                        </div>
                        <div className="metro">
                          <div className="metro-icon"><img src={metroIcon} /></div>
                          <div className="metro-name">{building.building.stations[0].name}</div>
                        </div>
                      </div>
                    </div>
                  </div>)
          }

        })}



        </div>
      </div>
    );
  }
}
