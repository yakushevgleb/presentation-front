import React, { Component } from 'react';
import TextDate from "../../components/Global/TextDate";

import titleBg from "../../../assets/img/1pg_bg.png";
import logo from "../../../assets/img/logo.png"
import ToggledInput from '../../components/Global/ToggledInput';

export default class TitlePage extends Component {

  constructor(props) {
    super(props);
    this.onChange = this.onChange.bind(this);
  }

  onChange(name, index, arrName, event) {
    let first_slide = this.props.model;
    first_slide[name] = event.target.value;
    this.props.changedData({first_slide});
  }

  render() {

    return (
      <div className='TitlePage'>
        <img src={titleBg} />
        <img src={logo} id="logo"/>
        <div className="title">
        <span className="bold">Офисная</span> <span className="thin">недвижимость</span>
        <span className="small-text thin">
          Презентация для компании "<ToggledInput value={this.props.model.clientName} fieldName="clientName" onChange={this.onChange} />"
        </span>
        <span className="date thin"><TextDate /></span>
        </div>
      </div>
    );
  }
}
