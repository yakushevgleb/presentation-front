import React, { Component } from 'react';
import { YMaps, Map, GeoObject, Placemark } from 'react-yandex-maps';
import Footer from "../../components/Global/Footer";

const mapState = { center: [55.76, 37.64], zoom: 10 };

export default class OnMap extends Component {
  state = { showMap: true };

  toggleMap() {
    const { showMap } = this.state;
    this.setState({ showMap: !showMap });
  }

  render() {
    const { showMap } = this.state;

    return (
      <div className="OnMap">
        <div className="map-area">
         <div className="map">
         </div>
        </div>
        <div className="text-area">
          <div className="page-title">
             Объекты <span className="bold">на карте</span>
          </div>
          <ul className="points-list">
            <li className="point"><div className="round-marker">1</div>Sdkdlads asdklalsdk naklsdalsd 21</li>
            <li className="point"><div className="round-marker">2</div>Sdkdlads asdklalsdk naklsdalsd 21</li>
          </ul>
        </div>
        <Footer />
      </div>
    );
  }
}
