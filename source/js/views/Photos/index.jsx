import React, { Component } from 'react';
import Config from 'Config';

import ImagePicker from '../../components/Global/ImagePicker'

export default class Photos extends Component {

  itemSelected(item, index, event) {
    let buildings = this.props.buildings;
    index = this.props.subArrIndex * this.props.model.length + index;
    buildings[this.props.buildingIndex].blocks[this.props.index][this.props.fieldName][index].show = item.show;;
    this.props.changedData({buildings});
  }


  render() {

    let images = this.props.model;
    let buildingName = this.props.buildings[this.props.buildingIndex].building.name;
    let block = this.props.buildings[this.props.buildingIndex].blocks[this.props.index];
    let sortedImgs = [];
    let showedCount = 0;

    function imgsOnPage(length) {
      let style = "";
      if(length === 1) {
        style = "one-img";
      }
      else if(length === 2) {
        style = "two-img";
      }
      else if(length > 2 && length <= 4) {
        style = "four-img";
      }
      else if (length > 4 && length <= 6) {
        style = "six-img";
      }
      else if (length > 6) {
        style = "eight-img";
      }
      return style;
    };

    function resizedImg(img, length) {
      let height = 315;
      let width = null;
      let fullUrl = Config.photoAddress + img;
      if (length === 1) {
        height = 650;
        width = 1200;
      }
      else if (length >= 2 && length <= 4) {
        width = 580;
      }
      else if (length > 4 && length <= 6) {
        width = 380;
      }
      else if (length > 6) {
        width = 280;
      }
      return `${Config.address}/api/presentations/data/image_crop?url=${fullUrl}&width=${width}&height=${height}`;
    };

    images.forEach((item, index)=>{
      if(item.show) {
        showedCount++;
      }
      if(showedCount > 8) {
        item.show = false;
      }
      sortedImgs.push(item);
    });

    let blockTitleName = "";
    if(this.props.fieldName === "images") {
      blockTitleName = "Фотографии офиса";
    } else {
      blockTitleName = "Планировка офиса";
    }

    let imgSize = "img-box " + imgsOnPage(showedCount);

    return (
      <div className="Photos">
        <div className="page-title thin">{buildingName}</div>
        <div className="block-title thin">{blockTitleName} {block.floors} этаж {block.area} м<sup>2</sup></div>
        {this.props.subArrIndex === 0 &&
          <ImagePicker list={block[this.props.fieldName]} itemSelected={this.itemSelected.bind(this)} />
        }
        <div className="photos-area">
          {this.props.model.map((image, index)=>{
            if (image.show) {
              if(this.props.fieldName === "images") {
                return (<div key={index} className={imgSize}>
                          <div className="photo-cover">
                            <img src={resizedImg(image.url, showedCount)} />
                          </div>
                        </div>)
              } else {
                return (<div key={index} className={"plans-box " + imgsOnPage(images.length)}>
                            <img src={Config.photoAddress + image.url} />
                        </div>)
              }

            }
          }
          )}
        </div>
      </div>
    );
  }
}
