import React, { Component } from 'react';
import Config from 'Config';

import TitlePage from '../TitlePage';
import SuggestedObjects from '../SuggestedObjects';
import OnMap from '../OnMap';
import AboutObject from '../AboutObject';
import Photos from '../Photos';
import Contacts from '../Contacts';
import Menu from '../../components/Global/Menu';
import Footer from '../../components/Global/Footer';

let pageCounter = 1;

export default class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isSaved: false
    }
    this.save = this.save.bind(this);
    this.changedData = this.changedData.bind(this);
  }

  componentDidMount() {

    let id = this.props.params["id"]

    fetch(`${Config.address}/api/presentations/data/landing_data?id=${id}`,
    {
      mode: 'cors'
    })
    .then(res => {
      res.json().then((data)=>{
        console.log(data);
        this.setState({ model: data });
      })
    });

  }

  changedData(obj) {
    let model = this.state.model;
    model = Object.assign(model, obj);
    this.setState({
      model: model,
      isSaved: false
    });
  }

  save() {
    let data = new FormData();
    let model = this.state.model;
    data.append("config", JSON.stringify(model))
    let id = this.props.params["id"]
    fetch(`${Config.address}/api/presentations/data/merge_config?id=${id}`,
    {
      method: "POST",
      body: data,
      mode: "cors"
    })
    .then(res=>{
      if(res.status === 200) {
        this.setState({isSaved: true});
      } else {
        console.log(res.status);
      }
    })
  }

  render() {

    function pagesWithImages(arr) {
      let separatedArr = [];
      let counter = 0;
      arr.sort(function(a, b){
        if (!a.show && b.show) {
          return 1;
        }
        if (a.show && b.show) {
          return -1;
        }
        if (a.show && !b.show) {
          return -1;
        }
        if (!a.show && !b.show) {
          return -1;
        }
      });
      arr.forEach((el, index)=>{
        if (counter === 5) {
          separatedArr[separatedArr.length - 1].push(el);
          counter = 0;
        }
        else if (counter === 0) {
          separatedArr.push([el]);
          counter++;
        } else {
          separatedArr[separatedArr.length - 1].push(el);
          counter++;
        }
      });

      return separatedArr;
    };

    function showPage(arr) {
      for (let i = 0; i < arr.length; i++) {
        if (arr[i].show) {
          return true;
        }
      }
      return false;
    };

    return (
      <div>
      {this.state.model ? (
        <div className='App'>
          <Menu save={this.save} isSaved={this.state.isSaved} />
          <div className='A4'><TitlePage model={this.state.model.first_slide} changedData={this.changedData} /></div>
          <div className='A4'>
            <SuggestedObjects model={this.state.model.buildings} />
            <Footer pageCounter={pageCounter} broker={this.state.model.broker} />
          </div>
          {this.state.model.buildings.map((building, buildingIndex)=>{
            if (buildingIndex < 10) {
              return (<div key={buildingIndex}>
                        <div className='A4'>
                          <AboutObject showFullInfo={true} orderType={this.state.model.order_type} buildings={this.state.model.buildings} model={building.building} blocks={building.blocks} index={buildingIndex} changedData={this.changedData} />
                          <Footer pageCounter={pageCounter += 1} broker={this.state.model.broker} />
                        </div>
                        {building.blocks.length > 5 &&
                          <div className='A4' key={buildingIndex}>
                            <AboutObject showFullInfo={false} orderType={this.state.model.order_type} buildings={this.state.model.buildings} model={building.building} blocks={building.blocks} index={buildingIndex} changedData={this.changedData} />
                            <Footer pageCounter={pageCounter += 1} broker={this.state.model.broker} />
                          </div>
                        }
                        {building.blocks.map((block, blockIndex)=>{
                          return (<div key={blockIndex}>
                                    {pagesWithImages(block.images).map((arr, index)=>{
                                      if (showPage(arr) || index === 0) {
                                        return (<div className='A4' key={index}>
                                                  <Photos
                                                    buildings={this.state.model.buildings}
                                                    model={arr}
                                                    index={blockIndex}
                                                    buildingIndex={buildingIndex}
                                                    fieldName="images"
                                                    subArrIndex={index}
                                                    changedData={this.changedData} />
                                                  <Footer pageCounter={pageCounter += 1} broker={this.state.model.broker} />
                                                </div>)
                                      }
                                    }
                                  )}
                                  {pagesWithImages(block.plans).map((arr, index)=>{
                                    if (showPage(arr) || index === 0) {
                                      return (<div className='A4' key={index}>
                                                <Photos
                                                  buildings={this.state.model.buildings}
                                                  model={arr}
                                                  index={blockIndex}
                                                  buildingIndex={buildingIndex}
                                                  fieldName="plans"
                                                  subArrIndex={index}
                                                  changedData={this.changedData} />
                                                <Footer pageCounter={pageCounter += 1} broker={this.state.model.broker} />
                                              </div>)
                                    }
                                  }
                                )}
                                </div>)

                        }
                        )}
                      </div>)
            }
          })}
          <div className='A4'><Contacts model={this.state.model.broker} changedData={this.changedData} /></div>
        </div>
      ) : (
        <div className='App'>
          <span className="loading">Загрузка...</span>
        </div>
      )}

      </div>
    );
  }
}
